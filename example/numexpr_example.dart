import 'package:numexpr/numexpr.dart';

void main() {
  //var bad = new Expression() ;
  var expr = new AddExpression();
  var expr2 = new SubExpression();
  //bad.Show(); 
  //bad.Execute() ;
  expr.setLeft(5) ;
  expr.setRight(6) ;
  expr.Show();
  //expr.Execute() ;
  //expr.Show() ;
  expr2.setLeft(7) ;
  expr2.setRight(expr) ;
  expr2.Show();
  print("${expr2.Evaluate()}\n");
  print("${expr2.getImage()}\n");

  //
  print("${expr.getImage()}\n");
  print("${expr.Evaluate()}\n");
  var gen = Generator() ;
  // gen.Next().Show();
  print("${gen.Next().getImage()}\n") ;

  var e1 = gen.Next() ;
  var e2 = gen.Next() ;
  var e3 = gen.NextwBoth(e1,e2) ;

  print("${e3.getImage()} = ${e3.Evaluate()}\n");

  var e4 = e3.Evaluate() ;
  print("Type of result ${e4.runtimeType} ${e4.toStringAsFixed(2)}\n");
}
