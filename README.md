# Numerical Expression Generator

In this projectlet, we develop an arithmentic expression generator. One of the 4 fundamental arithmentic operators is chosen at random. The operator can be applied to 2 randomly generated numbers ie a left and a right. Either of the operands can also be another expression. The expression is evaluated using the usual rules of precedence. Just to eliminate any confusions, the "package" can also provide a parenthetized "image" of the expression so generated.

## Use cases

Primarily this is developed to enable generating arithmetic problems and a student can be asked to evaluate it and provided a score. The more advanced student may be shown the problem for a limited time and given some time to provide a solution in order to earn a score.
## Language, Presentation and Platforms

The example is developed using \textbf{dart} with a view to plug this into a flutter app. The direction is to implement it for all the platforms that flutter will support.
## Usage

The example directory contains a simple example.

