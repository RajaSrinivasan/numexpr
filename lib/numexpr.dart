/// Support for doing something awesome.
///
/// More dartdocs go here.
library numexpr;

export 'src/expressions.dart';
export 'src/generator.dart';

// TODO: Export any libraries intended for clients of this package.
