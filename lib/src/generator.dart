import 'dart:math' ;
import 'expressions.dart';

class Generator extends Expression {
    var rnd = Random() ;
    Generate() {
    }

    isBetween(val,from,to) {
        if (val < from) return false ;
        if (val > to)   return false ;
        return true ;
    }

    Next() {
        int val1 = rnd.nextInt(99) ;
        int val2 = rnd.nextInt(99) ;
        double operval = rnd.nextDouble() ;
        if (isBetween(operval,0.0,0.25)) {
            var expr = AddExpression() ;
            expr.setLeft(val1) ;
            expr.setRight(val2) ;
            return expr ;
        }
        if (isBetween(operval,0.25,0.5)) {
            var expr = SubExpression() ;
            expr.setLeft(val1) ;
            expr.setRight(val2) ;
            return expr ;
        }
        if (isBetween(operval,0.5,0.75)) {
            var expr = MulExpression() ;
            expr.setLeft(val1) ;
            expr.setRight(val2) ;
            return expr ; 
        } 
        var expr = DivExpression() ;
        expr.setLeft(val1) ;
        expr.setRight(val2) ;
        return expr;
        
    }
    NextwLeft(other) {
        var expr = Next() ;
        expr.setLeft(other) ;
        return expr ;
    }
    NextwRight(other) {
        var expr = Next() ;
        expr.setRight(other) ;
        return expr ;
    }    
    NextwBoth(otherl,otherr) {
        var expr = Next() ;
        expr.setLeft(otherl) ;
        expr.setRight(otherr) ;
        return expr ;
    }
}