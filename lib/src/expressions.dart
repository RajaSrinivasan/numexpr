// TODO: Put public facing types in this file.

enum Operation {
  Unspecified ,
  Add ,
  Subtract ,
  Multiply ,
  Divide ,
  Number ,
}

class Expression {
  var _oper = Operation.Unspecified ;
  var _left, _right ;
  var _value=0 ;
  void Show() {
    print("${_oper} Left ${_left} Right ${_right} Value ${_value}\n");
    print("Types _oper:${_oper.runtimeType} _left:${_left.runtimeType} _right:${_right.runtimeType}\n");
  }
  
  void setLeft(val) {
    _left = val ;
  }

  getLeft() {
    if (_left.runtimeType == int) {
      return _left ;
    }
    return _left.Evaluate() ;
  }
  void setRight(val) {
    _right = val ;
  }

  getRight() {
    if (_right.runtimeType == int) {
      return _right ;
    } else {
      return _right.Evaluate() ;
    }
  }

  getImage() {
    var limage,rimage;
    if (_left.runtimeType == int) {
      limage = _left.toString() ;
    } else {
      limage = _left.getImage() ;
    }

    if (_right.runtimeType == int) {
      rimage = _right.toString() ;
    } else {
      rimage = _right.getImage() ;
    }
    return '(' + limage + OperImage() + rimage + ')' ;
    //return limage + rimage ;
  }

  OperImage() {
      return "?" ;
  }
}

class AddExpression extends Expression {
  AddExpression() {
    _oper = Operation.Add ;
  }

  Evaluate() {
    return getLeft() + getRight() ;
  }

  OperImage() {
      return "+" ;
  }
}

class SubExpression extends Expression {
  SubExpression() {
    _oper = Operation.Subtract ;
  }

  Evaluate() {
    return getLeft() - getRight() ;
  }

  OperImage() {
      return "-" ;
  }

}


class MulExpression extends Expression {
  MulExpression() {
    _oper = Operation.Multiply ;
  }

  Evaluate() {
    return getLeft() * getRight() ;
  }

  OperImage() {
      return "*" ;
  }

}



class DivExpression extends Expression {
  DivExpression() {
    _oper = Operation.Divide ;
  }

  Evaluate() {
    return getLeft() / getRight() ;
  }

  OperImage() {
      return "/" ;
  }

}

